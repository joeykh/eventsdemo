# Events demo app

This quick demo app was developed following an MVVM/Clean code approach focused on separation of concerns an ease of testing.

* The app shows a list of events coming from a web service.
* Due to a limitation of the API, infinite scrolling was not implemented, instead on launch all of the events are fetched by calling the API endpoint on loop until the last page.
* Overlapping events are indicated to the user by showing the name of the events in red.
* Events with missing title are labeled as "Untitled"
* Api "random" errors are handled by showing an alert and letting the user retry the request.

Given more time I would have improved on:

* Optimizing the processing of the data (sorting, detecting overlaps)
* Handling the cases where data doesn't make sense (for example end date being earlier than start)
* Writing more tests for the overlapping cases or other edge cases