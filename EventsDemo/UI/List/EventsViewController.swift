//
//  EventsViewController.swift
//  EventsDemo
//
//  Created by Joey on 4/12/21.
//

import UIKit

class EventsViewController: UITableViewController {

    let viewModel: EventListViewModel!

    var dayViewModels: [EventDayViewModel] = []

    init(viewModel: EventListViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationController?.navigationBar.prefersLargeTitles = true
        title = "Events"

        tableView.register(EventCell.self, forCellReuseIdentifier: EventCell.identifier)

        bindViewModel()
        viewModel.fetchEvents()
    }

    func bindViewModel() {
        viewModel.eventDays.bind { [weak self] eventDays in
            guard let eventDays = eventDays else { return }
            self?.dayViewModels = eventDays
            self?.tableView.reloadData()
            self?.viewModel.fetchEvents()
        }

        viewModel.eventsError.bind { [weak self] error in
            guard let error = error else { return }
            var message = ""
            switch error {
            case .apiError(let apiMessage):
                message = apiMessage
            default:
                message = error.localizedDescription
            }
            let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .cancel))
            alert.addAction(
                UIAlertAction(
                    title: "Retry",
                    style: .default,
                    handler: { [weak self] _ in
                        self?.viewModel.fetchEvents()
                    }
                )
            )
            self?.present(alert, animated: true)
        }
    }

}

extension EventsViewController {

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: EventCell.identifier) as? EventCell else {
            return UITableViewCell()
        }

        let eventViewModel = dayViewModels[indexPath.section].events[indexPath.row]
        cell.configure(with: eventViewModel)
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let eventViewModel = dayViewModels[indexPath.section].events[indexPath.row]
        let vc = EventDetailVC(viewModel: eventViewModel)
        navigationController?.pushViewController(vc, animated: true)
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let headerTitle = dayViewModels[section].startDay.formatted(date: .abbreviated, time: .omitted)
        return headerTitle
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dayViewModels[section].events.count
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return dayViewModels.count
    }
    
}
