//
//  EventCell.swift
//  EventsDemo
//
//  Created by Joey on 4/12/21.
//

import Foundation
import UIKit

class EventCell: UITableViewCell {

    static let identifier = "EventCell"

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }

    let titleLabel: UILabel = {
        let label = UILabel()
        label.font = .boldSystemFont(ofSize: 16)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    let startLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 14)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    let endLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 14)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    func commonInit() {
        selectionStyle = .none
        let stack = UIStackView(arrangedSubviews: [titleLabel, startLabel, endLabel])
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.axis = .vertical

        contentView.addSubview(stack)

        NSLayoutConstraint.activate([
            stack.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8),
            stack.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -8),
            stack.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            stack.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16)
        ])
    }

    func configure(with model: EventViewModel) {
        titleLabel.text = model.title
        startLabel.text = model.start.formatted()
        endLabel.text = model.end.formatted()
        titleLabel.textColor = model.overlapsWithPrevious ? .red : .label
    }

}
