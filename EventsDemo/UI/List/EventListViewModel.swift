//
//  EventsViewModel.swift
//  EventsDemo
//
//  Created by Joey on 4/12/21.
//

import Foundation

class EventListViewModel {

    let eventsApi: EventsApi!

    var events: [Event] = []

    let eventDays = Observable<[EventDayViewModel]>(nil)
    let eventsLoading = Observable<Bool>(false)
    let eventsError = Observable<EventsApiError>(nil)

    var nextPageToken: String? = nil

    init(eventsApi: EventsApi) {
        self.eventsApi = eventsApi
    }

    func fetchEvents() {
        if nextPageToken != nil && nextPageToken!.isEmpty {
            return
        }
        eventsLoading.value = true
        eventsApi.fetchEvents(pageToken: nextPageToken) { [weak self] result in
            self?.eventsLoading.value = false
            switch result {
            case .success(let response):
                self?.events.append(contentsOf: response.events ?? [])
                self?.events.sort(by: {
                    if $1.start == nil || $0.start == nil { return false }
                    return $1.start! > $0.start!
                })
                var data: [Date: [EventViewModel]] = [:]

                var prevEndDate: Date? = nil
                self?.events.forEach { event in
                    guard let day = event.day else { return }
                    if data[day] == nil {
                        data[day] = []
                    }
                    guard let startDate = event.start, let endDate = event.end else { return }
                    var eventTitle = "Untitled"
                    if let title = event.title, !title.isEmpty {
                        eventTitle = title
                    }
                    let overlaps = prevEndDate == nil ? false : prevEndDate! > startDate
                    data[day]?.append(EventViewModel(title: eventTitle, start: startDate, end: endDate, overlapsWithPrevious: overlaps))
                    prevEndDate = endDate
                }

                let eventDays = data.compactMap{ startDay, events in
                    EventDayViewModel(startDay: startDay, events: events.sorted(by: { $1.start > $0.start }))
                }
                
                self?.nextPageToken = response.nextPageToken
                self?.eventDays.value = eventDays.sorted(by: { $1.startDay > $0.startDay }) // Sorting here is not ideal but it shouldn't be a problem when the API is changed
            case .failure(let error):
                print(error)
                self?.eventsError.value = error
            }
        }
    }

}
