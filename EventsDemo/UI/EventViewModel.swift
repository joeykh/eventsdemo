//
//  EventCellViewModel.swift
//  EventsDemo
//
//  Created by Joey on 4/12/21.
//

import Foundation

struct EventDayViewModel {
    let startDay: Date
    let events: [EventViewModel]
}

struct EventViewModel {
    let title: String
    let start: Date
    let end: Date
    let overlapsWithPrevious: Bool
}
