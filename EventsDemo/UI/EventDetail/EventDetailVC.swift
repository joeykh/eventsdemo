//
//  EventDetailVC.swift
//  EventsDemo
//
//  Created by Joey on 4/12/21.
//

import Foundation
import UIKit

class EventDetailVC: UIViewController {

    let viewModel: EventViewModel!

    init(viewModel: EventViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        title = viewModel.title
    }

}
