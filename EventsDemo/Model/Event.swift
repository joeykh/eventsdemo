//
//  Event.swift
//  EventsDemo
//
//  Created by Joey on 4/12/21.
//

import Foundation

struct Event: Decodable {
    let title: String?
    let start: Date?
    let end: Date?

    var day: Date? {
        return start?.withoutTime()
    }
}
