//
//  Date+Extension.swift
//  EventsDemo
//
//  Created by Joey on 4/12/21.
//

import Foundation

extension Date {

    func withoutTime() -> Date {
        return Calendar.current.startOfDay(for: self)
    }

}
