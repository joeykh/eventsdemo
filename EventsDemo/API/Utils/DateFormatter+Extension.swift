//
//  DateFormatter+Extension.swift
//  EventsDemo
//
//  Created by Joey on 4/12/21.
//

import Foundation

extension DateFormatter {

    static let awairDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMMM d, yyyy h:mm a"
        formatter.calendar = Calendar(identifier: .gregorian)
        formatter.locale = Locale(identifier: "en_US_POSIX")
        return formatter
    }()

}
