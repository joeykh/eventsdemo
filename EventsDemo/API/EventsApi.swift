//
//  EventsApi.swift
//  EventsDemo
//
//  Created by Joey on 4/12/21.
//

import Foundation

enum EventsApiError: Error {
    case invalidURL
    case notAvailable
    case parsingError
    case apiError(message: String)
}

protocol EventsApi {
    func fetchEvents(pageToken: String?, completion: @escaping (Result<EventApiResponse, EventsApiError>) -> ())
}
