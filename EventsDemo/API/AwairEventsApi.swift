//
//  AwairEventsApi.swift
//  EventsDemo
//
//  Created by Joey on 4/12/21.
//

import Foundation

class AwairEventsApi: EventsApi {

    private static let apiEndpoint = "http://mobile-app-interview.awair.is/events"

    let urlSession: URLSession!

    let jsonDecoder: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .formatted(.awairDateFormatter)
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        return decoder
    }()

    public init(urlSession: URLSession) {
        self.urlSession = urlSession
    }

    public func fetchEvents(pageToken: String?, completion: @escaping (Result<EventApiResponse, EventsApiError>) -> ()) {
        var urlString = AwairEventsApi.apiEndpoint
        if let pageToken = pageToken {
            urlString += "?next_page_token=\(pageToken)"
        }
        guard let url = URL(string: urlString) else {
            completion(.failure(.invalidURL))
            return
        }

        print("Calling: \(url.absoluteString)")
        urlSession.dataTask(with: url) { [jsonDecoder] data, urlResponse, error in
            guard let data = data else {
                completion(.failure(.notAvailable))
                return
            }
            do {
                let results = try jsonDecoder.decode(EventApiResponse.self, from: data)
                if let apiErrorMessage = results.error, !apiErrorMessage.isEmpty {
                    return completion(.failure(.apiError(message: apiErrorMessage)))
                }
                completion(.success(results))
            } catch {
                print(error)
                completion(.failure(.parsingError))
            }
        }.resume()
    }

}
