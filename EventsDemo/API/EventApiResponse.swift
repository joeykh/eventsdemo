//
//  EventApiResponse.swift
//  EventsDemo
//
//  Created by Joey on 4/12/21.
//

import Foundation

struct EventApiResponse: Decodable {
    let error: String?
    let events: [Event]?
    let nextPageToken: String?
}
