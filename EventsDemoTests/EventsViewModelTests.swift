//
//  EventsViewModelTests.swift
//  EventsDemoTests
//
//  Created by Joey on 4/12/21.
//

import XCTest
@testable import EventsDemo

class EventsViewModelTests: XCTestCase {

    let mockEventsApi = MockEventsApi()
    lazy var sut: EventListViewModel = EventListViewModel(eventsApi: mockEventsApi)

    func testFetchEvents_ValidResult_ViewModelsReturned() {
        let fakeEvent = Event(title: "Fake Title", start: Date(), end: Date())
        mockEventsApi.fakeEvents = [fakeEvent]

        let expectation = XCTestExpectation(description: "Same title returned")

        sut.fetchEvents()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [sut] in
            XCTAssertEqual(sut.eventDays.value?.first?.events.first?.title, "Fake Title")
            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 2)
    }

    func testFetchEvents_EventWithEmptyTilte_UntitledEventWitReturned() {
        let fakeEvent = Event(title: "", start: Date(), end: Date())
        mockEventsApi.fakeEvents = [fakeEvent]

        let expectation = XCTestExpectation(description: "Title will be 'Untitled'")

        sut.fetchEvents()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [sut] in
            XCTAssertEqual(sut.eventDays.value?.first?.events.first?.title, "Untitled")
            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 2)
    }

    func testFetchEvents_EventWithNilTilte_UntitledEventWitReturned() {
        let fakeEvent = Event(title: nil, start: Date(), end: Date())
        mockEventsApi.fakeEvents = [fakeEvent]

        let expectation = XCTestExpectation(description: "Title will be 'Untitled'")

        sut.fetchEvents()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [sut] in
            XCTAssertEqual(sut.eventDays.value?.first?.events.first?.title, "Untitled")
            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 2)
    }

    func testFetchEvents_EventWithNilInfo_EventSkipped() {
        let fakeEvent = Event(title: nil, start: nil, end: nil)
        mockEventsApi.fakeEvents = [fakeEvent, Event(title: "Test", start: Date(), end: Date())]

        let expectation = XCTestExpectation(description: "Events array count will be 1, title will be 'Test'")

        sut.fetchEvents()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [sut] in
            XCTAssertTrue(sut.eventDays.value?.count == 1)
            XCTAssertEqual(sut.eventDays.value?.first?.events.first?.title, "Test")
            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 2)
    }

}

class MockEventsApi: EventsApi {

    var fakeError: EventsApiError?
    var fakeApiErrorMessage: String?
    var fakeEvents: [Event]?
    var pageToken: String?

    func fetchEvents(pageToken: String?, completion: @escaping (Result<EventApiResponse, EventsApiError>) -> ()) {
        if let fakeError = fakeError {
            completion(.failure(fakeError))
        }
        let fakeRepsonse = EventApiResponse(error: fakeApiErrorMessage, events: fakeEvents, nextPageToken: pageToken)
        return completion(.success(fakeRepsonse))
    }

}
